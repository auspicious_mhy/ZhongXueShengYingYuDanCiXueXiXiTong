package translate1;

import java.awt.*;
import java.io.*;
import java.awt.event.*;
import javax.swing.*;

public class WriteFile extends JFrame {
	public static String enstr = new String("");
	public static String chstr = new String("");
	public static Point pressedPoint;

	public void show() {
		try {
			String systemname = UIManager.getSystemLookAndFeelClassName();
			UIManager.setLookAndFeel(systemname);
		} catch (Exception e) {

		}
		String[] gradestr = { "初一上册", "初一下册", "初二上册", "初二下册", "初三上册", "初三下册" };
		JComboBox grade = new JComboBox(gradestr);
		grade.setBounds(20, 50, 260, 30);

		String[] unitstr = { "unit1", "unit2", "unit3", "unit4", "unit5", "unit6", "unit7", "unit8" };
		JComboBox unit = new JComboBox(unitstr);
		unit.setBounds(290, 50, 100, 30);

		JFrame write = new JFrame("写入翻译文件数据");
		Container writec = write.getContentPane();

		JTextField en = new JTextField("英文");
		en.setBounds(20, 100, 460, 30);
		en.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent arg0) {
				en.setText("");
			}

			public void focusLost(FocusEvent arg0) {

			}
		});

		JTextField ch = new JTextField("中文");
		ch.setBounds(20, 140, 460, 30);
		ch.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent arg0) {
				ch.setText("");
			}

			public void focusLost(FocusEvent arg0) {

			}
		});

		JButton enter = new JButton("插入至词库");
		enter.setBorderPainted(false);
		enter.setBounds(20, 180, 270, 30);
		enter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				enstr = en.getText();
				chstr = ch.getText();
				String getgrade = new String(grade.getSelectedItem().toString());
				String getunit = new String(unit.getSelectedItem().toString());
				File outdir = new File("translate/");
				File outfile = new File("translate/" + getgrade + getunit + enstr + ".translate");
				if (!outfile.exists()) {
					try {
						outdir.mkdirs();
						outfile.createNewFile();
						FileWriter translatefilewrite = new FileWriter(outfile, true);
						translatefilewrite.write(chstr + "\r\n");
						translatefilewrite.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					int number1 = JOptionPane.showOptionDialog(null, "翻译文件已存在，请选择操作", "错误",
							JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null,
							new String[] { "保留原样(不操作)", "删除原文件", "取消" }, "取消");// 保留原样是0,删除原文件是1，取消是2
					if (number1 == 0) {
						en.setText("");
						ch.setText("");
					} else if (number1 == 1) {
						outfile.delete();
					}
				}
			}
		});

		JButton delete = new JButton("从词库中删除数据");
		delete.setBorderPainted(false);
		delete.setBounds(310, 180, 170, 30);
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File delfile1 = new File("translate/");
				String delname = JOptionPane.showInputDialog("输入要删除英文单词");
				delname.trim();
				File delfile2 = new File(delfile1 + "/" + delname + ".translate");
				delfile2.delete();
			}
		});

		JButton mix = new JButton("关闭");
		mix.setBorderPainted(false);
		mix.setBounds(20, 220, 460, 30);
		mix.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				write.setExtendedState(JFrame.ICONIFIED);
				write.setVisible(false);
			}
		});

		write.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				pressedPoint = e.getPoint();
			}
		});

		write.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				Point translatepoint = e.getPoint();
				Point lastpoint = write.getLocation();
				int translatex = lastpoint.x + translatepoint.x - pressedPoint.x;
				int translatey = lastpoint.y + translatepoint.y - pressedPoint.y;
				write.setLocation(translatex, translatey);
			}
		});

		writec.add(en);
		writec.add(ch);
		writec.add(enter);
		writec.add(mix);
		writec.add(delete);
		writec.add(grade);
		writec.add(unit);
		write.setLayout(null);
		writec.setBackground(new Color(102, 204, 255));
		write.setBounds(330, 10, 500, 400);
		write.setUndecorated(true);
		write.setVisible(true);

	}
}
