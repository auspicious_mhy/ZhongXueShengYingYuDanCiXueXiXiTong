package translate1;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

public class MainPane {
	public static String in = new String();
	public static File file1 = new File("translate/defaultfile");
	public static File file2 = new File("translate/userfile");
	public static File file = file1;
	public static String enstr = new String();
	public static String chstr = new String();
	public static Point pressedPoint;
	public static File enfile = new File("");

	public static void main(String[] args) {
		try {
			String systemname = UIManager.getSystemLookAndFeelClassName();
			UIManager.setLookAndFeel(systemname);
		} catch (Exception e) {
		}
		JFrame translate = new JFrame("����");
		Container translatec = translate.getContentPane();

		final JTextField infield = new JTextField();
		infield.setBounds(75, 125, 160, 30);

		final JTextField outfield = new JTextField();
		outfield.setBounds(75, 165, 235, 30);
		outfield.setEditable(false);

		JLabel inlabel = new JLabel("Ӣ�ﵥ��:");
		inlabel.setBounds(10, 125, 65, 30);

		JLabel outlabel = new JLabel("����:");
		outlabel.setBounds(10, 165, 65, 30);

		String[] radiusstr = { "��һ�ϲ�", "��һ�²�", "�����ϲ�", "�����²�", "�����ϲ�", "�����²�" };
		JComboBox radius = new JComboBox(radiusstr);
		radius.setBounds(130, 70, 100, 30);

		JButton enter = new JButton("����");
		enter.setBounds(240, 125, 70, 30);
		enter.setBorderPainted(false);
		enter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enstr = infield.getText();
				String bookstr = new String(radius.getSelectedItem().toString());
				System.out.println(bookstr);
				try {
					File enfile1 = new File("translate/" + bookstr + "unit1" + enstr + ".translate");
					File enfile2 = new File("translate/" + bookstr + "unit2" + enstr + ".translate");
					File enfile3 = new File("translate/" + bookstr + "unit3" + enstr + ".translate");
					File enfile4 = new File("translate/" + bookstr + "unit4" + enstr + ".translate");
					File enfile5 = new File("translate/" + bookstr + "unit5" + enstr + ".translate");
					File enfile6 = new File("translate/" + bookstr + "unit6" + enstr + ".translate");
					File enfile7 = new File("translate/" + bookstr + "unit7" + enstr + ".translate");
					File enfile8 = new File("translate/" + bookstr + "unit8" + enstr + ".translate");
					if (enfile1.exists()) {
						enfile = enfile1;
					} else if (enfile2.exists()) {
						enfile = enfile2;
					} else if (enfile3.exists()) {
						enfile = enfile3;
					} else if (enfile4.exists()) {
						enfile = enfile4;
					} else if (enfile5.exists()) {
						enfile = enfile5;
					} else if (enfile6.exists()) {
						enfile = enfile6;
					} else if (enfile7.exists()) {
						enfile = enfile7;
					} else if (enfile8.exists()) {
						enfile = enfile8;
					}

					FileReader fr = new FileReader(enfile);
					char byt[] = new char[1024];
					int len = fr.read(byt);
					outfield.setText(new String(byt, 0, len));

				} catch (Exception e1) {
					if (infield.getText().length() == 0) {
						JOptionPane.showMessageDialog(null, "�����Ϊ��", "����", JOptionPane.ERROR_MESSAGE);
					} else if (!new File(file + "/" + infield.getText() + ".translate").exists()) {
						JOptionPane.showMessageDialog(null, "�������ݲ�����", "����", JOptionPane.ERROR_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(null, "���ִ���", "����", JOptionPane.ERROR_MESSAGE);
					}
				}
				//
			}
		});

		translate.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		translate.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				pressedPoint = e.getPoint();
			}
		});
		translate.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				Point translatepoint = e.getPoint();
				Point lastpoint = translate.getLocation();
				int translatex = lastpoint.x + translatepoint.x - pressedPoint.x;
				int translatey = lastpoint.y + translatepoint.y - pressedPoint.y;
				translate.setLocation(translatex, translatey);
			}
		});

		Font titlefont = new Font("����", Font.BOLD, 15);
		JLabel title = new JLabel("��ѧ��Ӣ�ﵥ��ѧϰϵͳ-��ѯ����");
		title.setFont(titlefont);
		title.setBounds(20, 0, 300, 50);

		JPanel radiuspane = new JPanel();
		radiuspane.setBackground(new Color(102, 204, 255));
		radiuspane.setBounds(110, 75, 80, 32);

		JButton insert = new JButton("���Ӵʿ�");
		insert.setBorderPainted(false);
		insert.setBounds(10, 210, 170, 30);
		insert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WriteFile wf = new WriteFile();
				wf.show();
			}
		});

		JButton exit = new JButton("�˳�");
		exit.setBorderPainted(false);
		exit.setBounds(190, 210, 120, 30);
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});

		translatec.add(radiuspane);
		translatec.add(infield);
		translatec.add(outfield);
		translatec.add(enter);
		translatec.add(title);
		translatec.add(inlabel);
		translatec.add(outlabel);
		translatec.add(insert);
		translatec.add(exit);
		translatec.add(radius);
		translate.setLayout(null);
		translate.setBounds(10, 10, 320, 400);
		translatec.setBackground(new Color(102, 204, 255));
		translate.setUndecorated(true);
		translate.setVisible(true);
	}
}